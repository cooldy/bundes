**note** -  added a readme.html just if you have some issues with the formatting of the lists
# Bundesliga Demo project
This is a demo project - part of an interview process
# Setup
  - Clone the project
  - Set .env file
    Copy the .env.example to .env and fill the Database info. Also ... create your database:
```env
DB_CONNECTION=mysql
DB_HOST=
DB_PORT=
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```
  - Install composer 
```sh
composer install
```
  - Run migrations 
```sh
php artisan migrate
```
  - Run commands to fill database (*Further explanations below*)
```sh
bundes:get_leagues //Gets all the leagues (needed for names and validation) from the matches requests by league and league year (parameters as array)

bundes:get_teams  //Gets all the teams by the given league and league year (parameters as array)

bundes:update_goalgetters {league} {year}  //Updates the goalgetters for given  league/year

bundes:update_matches  {league} {year} //Updates the matches for given  league/year

bundes:update_teams_stats  {league} {year} //Updates teams statistics based on league/year
```

# Documentation
The Project has 3 main parts:
 - The client 
The client is used to communicate with the API. It responsibilities are to get the data from the API and return it in certain format which depends on the call itself. The returned info is separated into entities which are objects for the speicific date type, for ex: MatchEntity - the object that is returned for the getMatches request
 - Scheduler
Responsible to get the data from the client and record it into the database. 
 - Web UI
Responsible to return HTML to the clients so they have a nice looking UI.

Link to **schema drawing** (drawing isn't my strong side) - [https://imgur.com/aAYC0La](https://imgur.com/aAYC0La)



## Reasoning
#### Why Laravel ? 
I'm using Laravel becuase the project is really similiar to a CMS (the future idea behind it). Most of the features needed in the project can be developed a lot faster with Laravel. Some of the main capabilities that I'm using from Laravel are:
- Blade templating
- Routes
- Eloquent ORM
- Console commands
- Composer out of the box
- Powerful service container

#### Why store the info in a database when we may have live info ? 
In future versions of the project we nede to have offline access to the data. That means we can't have Internet connections to the OpenligaDB API. 
**2nd reason** is that we need to be able to record notes,images, etc relateed to data that we have from the API. 
**3rd reason** is that we don't depend on the service itself. Of course we depend on it for the future results but not for the past.
**4th reason** we can have multiple APIs and we won't have any issue working with all of them. All of the data would be structured the same way for all of them so the WEB UI doesn't depend on that

**Cons**
- Old data
Our data isn't changed after the inital requests. **To fix** this we can have a scheduler that runs every minute/second/hour - depending on the needs - so we update the data that needs updates. For ex: Future matches that in the moment of the indexation of the API weren't finished, but now have been played and have finished. This means the data is different. Also Team stats, goalgetters, etc. Using similar method we can even have **live notifications** on our website. 
Of course this on it's own have other cons but I won't go into detail what are the cons of **cached data**. However I'd be happy to talk about in a meeting :) 

## Some info about the commands
**Get Leagues**
``` 
bundes:get_leagues
```
Gets all the leagues based on dummy data array for the seasons/leagues. I couldn't find a **get leagues**  call from the API so I decided to get it from the **get matches** call - It should be ran just once so we have some data to work with 


**Get Teams**
```
bundes:get_teams 
```
Gets the teams for the leagues. I saw some problems with the IDs so there's a warning thrown to investigate further if something goes bad.

**Update Goalgetters**
```
bundes:update_goalgetters {league} {year} 
```
Updates/inserts the goalgetters for a current league/season. "all" argument can be used for both of the parameters. This command can and should be used more than once to update all data. **All UPDATE commands are developed with the same thing in mind

**Update Matches**
```
bundes:update_matches  {league} {year} 
```
Updates/inserts the matches for a current league/season. "all" argument can be used for both of the parameters.

**Update Team stats**
```
bundes:update_teams_stats  {league} {year}
```
Updates/inserts team stats for a current league/season. "all" argument can be used for both of the parameters.


## P.S. 
I tried to keep this as short as possible but also to give the most important info about the project and the how I was thinking while developing it. I'd be glad to discuss any further questions about it. 
 