$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content'),
        }
    })
    $('.dy-datepicker').datepicker({
        language: "bg",
        autoclose: true,
        format: "yyyy-mm-dd"
    });
    $('.summernote').summernote({

        toolbar: [
            [
                'style', [
                    'bold', 'italic', 'underline', 'clear'
                ]
            ],
        ],
        height: 150
    });

    $('#getProductPriceForm').on('click', function(e) {
        e.preventDefault();
        $('[name="dy_size_id"]').parent().removeClass('has-danger'),
            // $('[name="type_id"]').parent().removeClass('has-danger'),
            $('[name="condition_id"]').parent().removeClass('has-danger'),
            $('[name="weight"]').parent().removeClass('has-danger'),
            $('[name="brand_id"]').parent().removeClass('has-danger')
        var self = $(this);
        var categories = [];
        $('input[name="categories[]"]:checked').map(function(smth, item) {
            return categories.push($(item).val());
        });
        self.hide();
        $.ajax({
                method: "GET",
                url: "/admin/products/getPrice",
                data: {
                    dy_size_id: $('[name="dy_size_id"]').val(),
                    categories: categories,
                    condition_id: $('[name="condition_id"]').val(),
                    weight: $('input[name="weight"]').val(),
                    brand_id: $('[name="brand_id"]').val(),
                    fashion_factor: $('[name="fashion_factor"]').val()
                }
            })
            .done(function(data) {
                console.log(data.price.pretty_price);
                $('.js-fill-recommended_price').val(data.price.pretty_price)
                $('.js-fill-calculated_price').val(data.price.calc_price)
                if (!$('input[name="old_price"]').val()) {
                    $('input[name="old_price"]').val(data.price.pretty_price)
                }
                if (!$('input[name="price"]').val()) {
                    $('input[name="price"]').val(data.price.pretty_price)
                }
                self.show();
            }).fail(function(resp) {
                $.each(resp.responseJSON.errors, function(index, value) {
                    $('[name="' + index + '"]').parent().addClass('has-danger');
                })
                self.show();
            });


    });
    $('.level-root>label>input[type="checkbox"]').on('change', function() {
        triggerCategories($(this), '.level-root', '.level-1');
    });
    $('.level-1>label>input[type="checkbox"]').on('change', function() {
        triggerCategories($(this), '.level-1', '.level-2');
    });
    $('.dy-dropzone').dropzone({
        init: function() {
            this.on('success', function(file, json) {
                $('#images_container').show().append('\
                    <div class="col-sm-3 image">\
                        <label for="">Ред:</label>\
                        <a href="new" class="delete">изтрий</a>\
                        <input type="text" name="sort_order[]" class="form-control">\
                        <input type="hidden" value="0" name="image_id[]" class="form-control">\
                        <input type="hidden" name="filename[]" value="' + json.file_name + '"/>\
                        <img class="mt10" src="' + json.path + '" width="100%" alt="">\
                    </div>\
                \
                ');
            });
        },
        sending: function(file, xhr, formData) {
            formData.append("_token", $('meta[name="csrf-token"]').attr('content'));
        },
    });

    $('.dy-dropzone-slider, .dy-dropzone-article, .dy-dropzone-brand ').dropzone({
        init: function() {
            this.on('success', function(file, json) {
                $('.image-container img').attr('src', '/images_data/original/' + json.page + '/' + json.file_name)
                $('form').append('<input type="hidden" name="filename" value="' + json.file_name + '"/>');
                $('button[type="submit"]').attr('disabled', false);
            });
        },
        sending: function(file, xhr, formData) {
            $('button[type="submit"]').attr('disabled', 'disabled');
            formData.append("_token", $('meta[name="csrf-token"]').attr('content'));
        },
    });

    $('#translate_product').on('click', function() {
        $.ajax({
                method: "GET",
                url: "/admin/products/getTranslations",
                data: {
                    title: $('[name="translations[bg][title]"]').val(),
                    description: $('[name="translations[bg][description]"]').val()
                }
            })
            .done(function(data) {
                $.each(data, function(lang, value) {
                    $('[name="translations[' + lang + '][title]"]').val(value.title);
                    $('[name="translations[' + lang + '][description]"]').html(value.description);
                    // $('[name="translations[' + lang + '][description]"]').summernote('code', value.description);
                })

            });
    });


    $('.table-responsive table').on('click', '.js-save_brand_class', function(e) {
        e.preventDefault();
        var self = $(this);
        var tr = self.closest('tr');
        var input = tr.find('input:checked').val();

        var brand_id = self.attr('data-row_selector');
        var data = {
            'brand_class': input
        }
        self.hide();

        $.ajax({
            method: 'post',
            url: '/admin/brands/' + brand_id + '/saveBrandClass',
            data: data
        }).done(function(data) {
            self.show();
            self.closest('tr').css('opacity', '0.4').find('input').attr('readonly', 'readonly');
        }).fail(function() {
            self.show();
        })
    })


    $('.table-responsive table').on('click', '.js-save_translation.single', function(e) {
        e.preventDefault();

        var self = $(this);
        self.hide()
        var $parent = $($(this).attr('data-row_selector'));
        var $inputs = $parent.find('input');
        var slug = $parent.find('.the_slug').attr('data-slug');
        console.log(self);
        var translation_type = self.attr('data-translation-type');
        var data_arr = [];
        data_arr = {};
        $inputs.each(function() {
            var lang = $(this).attr('data-lang');
            var value = $(this).val();
            data_arr['lang.' + lang] = value;
        });
        data_arr.slug = slug;
        $.ajax({
                method: "post",
                url: "/admin/translations/" + translation_type,
                data: data_arr
            })
            .done(function(data) {
                self.show()
            });
    });


    $('#add_phrases').on('click', function(e) {

        var newSlug = +$('#max_slug').val() + 1;
        $('#max_slug').val(newSlug);
        var type = $('#productsTranslation').attr('data-translation-type');

        let newTr = '<tr id="slug_' + newSlug + '"><td class="text-center"><span data-slug="' + newSlug + '" class="the_slug">' + newSlug + '</span></td><td<input data-lang="" type="text" class="form-control" name="translation[bg][' + newSlug + ']" value=""/></td><td><input data-lang="bg" type="text" class="form-control" name="translation[bg][' + newSlug + ']" value=""></td><td><input data-lang="ro" type="text" class="form-control" name="translation[ro][' + newSlug + ']" value=""></td><td><input data-lang="en" type="text" class="form-control" name="translation[en][' + newSlug + ']" value=""></td><td><a data-translation-type="' + type + '" data-row_selector="#slug_' + newSlug + '" class="js-save_translation single btn btn-brand m-btn m-btn--icon" href="#"><i class="f22 la la-save"></i></a></td></tr>';
        $('#productsTranslation tbody').append(newTr);
    });

    $('.table-responsive table').on('click', '.js-save_translation.phrases', function(e) {
        e.preventDefault();

        var self = $(this);
        self.hide()

        var $inputs = self.closest('tr').find('input');
        var data_arr = {};
        data_arr['slug'] = self.closest('tr').find('input[name="slug"]').val();

        console.log(data_arr);
        $inputs.each(function() {
            if ($(this).attr('data-lang') != undefined) {
                var lang = $(this).attr('data-lang');
                var value = $(this).val();
                data_arr['lang.' + lang] = value;
            }
        });

        $.ajax({
                method: "post",
                url: "/admin/translations/phrase",
                data: data_arr
            })
            .done(function() {
                self.show();
            });
    });

    $('#add_phrases_site').on('click', function(e) {
        let newTr = '<tr><td class="text-center"><input name="slug" type="text" class="form-control"/></td><td<input type="text" class="form-control" name="translation[bg]" value=""/></td><td><input data-lang="bg" type="text" class="form-control" name="translation[bg]" value=""></td><td><input data-lang="ro" type="text" class="form-control" name="translation[ro]" value=""></td><td><input data-lang="en" type="text" class="form-control" name="translation[en]" value=""></td><td><a class="js-save_translation phrases btn btn-brand m-btn m-btn--icon" href="#"><i class="f22 la la-save"></i></a></td></tr>';
        $('#productsTranslation tbody').append(newTr);
    });

    // PRODUCTS PRICE PARAMETERS

    $('#priceParameters').on('click', '.js-save_price_parameter', function(e) {
        e.preventDefault();
        var self = $(this);
        var tr = self.closest('tr');

        var pricingValue = tr.find('input[name="pricing_value"]').val();
        var paramValue = tr.find('select[name="param_value"]').val();
        var select = tr.find('select[name="priceParam"]').val();

        var param_id = self.attr('data-row_selector');

        var data = {
            'pricingValue': pricingValue,
            'paramValue': paramValue,
            'code': select,
            'id': param_id
        }
        self.hide();

        $.ajax({
            method: 'post',
            url: '/admin/products/' + param_id + '/priceParam',
            data: data
        }).done(function(data) {
            self.show();
            self.closest('tr').find('input[name="param_value"]').attr('readonly', 'readonly').css('opacity', '0.4');
            self.closest('tr').find('select').attr("disabled", true);
            location.reload();
        }).fail(function() {
            self.show();
        })
    })

    $()

    $('#add_price_param_row').on('click', function(e) {

        // Get Select option with ajax id and label
        var param;
        $.ajax({
            'async': false,
            'method': 'get',
            'url': '/admin/products/priceParam/getParam',
            'success': function(data) {
                param = data;
            }
        })

        var newId = +$('#max_slug').val() + 1;
        $('#max_slug').val(newId);

        let newTr = '<tr><td><select name="priceParam"  class="form-control" id="priceParam_' + newId + '"><option value="0">Избери параметър</option></select></td><td><select id="paramVal_' + newId + '" class="form-control" name="param_value"></select></td><td><input name="pricing_value" value="" /></td><td class="text-center"><a data-row_selector="' + newId + '" class="js-save_price_parameter single btn btn-brand m-btn m-btn--icon" href="#"><i class="f22 la la-save"></i></a></td></tr>';


        $('#priceParameters tbody').append(newTr);

        // Set all option to select
        param.forEach(element => {
            $('#priceParam_' + newId + '').append($('<option>', {
                value: element.code,
                text: element.label
            }));
        });

        $('#priceParam_' + newId + '').on('change', function() {
            var self = $(this);
            var sendData = {
                'code': self.val()
            }

            $.ajax({
                async: false,
                method: 'get',
                url: '/admin/products/priceParam/paramValueSelect',
                data: sendData
            }).done(function(data) {
                $('#paramVal_' + newId + '').find('option').remove().end();
                $.each(data, function(key, value) {
                    $('#paramVal_' + newId + '')
                        .append($('<option>', {
                            value: key,
                            text: value
                        }))
                })
            }).fail(function() {
                $('#paramVal_' + newId + '')
                    .find('option').remove().end().append($('<option>', {
                        value: 0,
                        text: 'Избери параметър'
                    }))
            })


        })


        $(this).hide();
    });

    $('#images_container').on('click', '.delete', function(e) {
        e.preventDefault();
        var $image_wrap = $(this).parent();
        if ($(this).attr('href') == 'new') {
            $image_wrap.remove();
        } else {
            // do ajax here
        }
        if (!$('#images_container .image').length) {
            $('#images_container').hide();
        }
    });
    $('.select2').select2();

    $('.select2-ajax').select2({
        ajax: {
            url: function(pars, a1, a2) {
                let url = $(this).attr('data-href');
                if ($(this).attr('data-site_dependant')) {
                    url += '?site_id=' + $('#shipping_spd_city_id').val()
                }
                return url;
            },
            processResults: function(data) {
                var arr = [];
                data.results.forEach((element, index) => {
                    arr[index] = {
                        id: element.id,
                        text: element.value,
                    }
                });

                return {
                    results: arr
                };
            },
            dataType: 'json'
        }
    });
    $('#shipping_spd_neighbourhood_id').on('select2:select', function(e) {
        var data = e.params.data;
        $('input[name="shipping_neighbourhood"]').val(data.text);

    })
    $('#shipping_spd_street_id').on('select2:select', function(e) {
        var data = e.params.data;
        $('input[name="shipping_street"]').val(data.text);

    })
    $('#shipping_spd_city_id').on('select2:select', function(e) {
        var data = e.params.data;
        $('#shipping_spd_office_id').empty()
        $('#shipping_spd_neighbourhood_id').empty()
        $('#shipping_spd_street_id').empty()

        $.ajax({
                method: "GET",
                url: "/speedy/offices",
                data: { site_id: data.id }
            })
            .done(function(data) {
                console.log('from ajax', data);

                var offices = [];
                offices[0] = {
                    id: 0,
                    text: "Изберете офис"
                }
                data.results.forEach((element, index) => {
                    offices.push({
                        id: element.id,
                        text: element.value,
                    })
                });
                $('#shipping_spd_office_id').select2({
                    data: offices
                });

            });
    });


    $('.menu-not-rdy').on('click', function(e) {
        e.preventDefault();
        return false;
    })
})
$(window).on('load', function() {
    triggerCategories($('.level-root>label>input[type="checkbox"]'), '.level-root', '.level-1');
    triggerCategories($('.level-1>label>input[type="checkbox"]'), '.level-1', '.level-2');
})

function triggerCategories($checkbox, parent_class, child_class) {
    $checkbox.each(function() {
        var $children = $(this).parents(parent_class).find(child_class);
        if ($(this).is(':checked')) {
            // $children.show();
        } else {
            // $children.hide();
        }
    });
}