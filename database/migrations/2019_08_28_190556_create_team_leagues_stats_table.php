<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamLeaguesStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams_leagues_stats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('league_id')->unsigned();
            $table->integer('team_id')->unsigned();
            $table->smallInteger('won')->unsigned();
            $table->smallInteger('lost')->unsigned();
            $table->smallInteger('draws')->unsigned();
            $table->smallInteger('goals')->unsigned();
            $table->smallInteger('matches')->unsigned();
            $table->smallInteger('opponent_goals')->unsigned();
            $table->smallInteger('points')->unsigned();
            $table->timestamps();

            $table->foreign('team_id')->references('id')->on('teams');
            $table->foreign('league_id')->references('id')->on('leagues');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams_leagues_stats');
    }
}
