<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoalgettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goalgetters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('league_id')->unsigned();
            $table->string('name');
            $table->smallInteger('goals')->unsigned();
            $table->timestamps();

            $table->foreign('league_id')->references('id')->on('leagues');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goalgetters');
    }
}
