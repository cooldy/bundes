<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoalgetterModel extends Model
{
    protected $table = 'goalgetters';
    protected $guarded = []; //yolo
}
