<?php

namespace App\Http\Controllers;

use App\LeagueModel;
use App\MatchModel;
use App\TeamLeagueStatModel;
use App\TeamModel;
use Illuminate\Http\Request;

class TeamsController extends Controller
{
    public function index(Request $request)
    {
        $leagues_collection = LeagueModel::all();
        $years = array_unique($leagues_collection->pluck('year')->toArray());
        $leagues = array_unique($leagues_collection->pluck('name', 'code')->toArray());

        $year = $request->get('year', config('bundes.default_year'));
        $league_code = $request->get('code', config('bundes.default_league'));
        $league = LeagueModel::where('year', $year)->where('code', $league_code)->first();
        $title = $league->name;

        $teams_stats = TeamLeagueStatModel::with('team')->where('league_id', $league->id)->orderBy('points', 'desc')->paginate(20);
        return view('teams.index', compact('title', 'teams_stats', 'leagues', 'years'));
    }

    public function show($team_id)
    {
        $team = TeamModel::find($team_id);
        $last10_matches = MatchModel::where('team1_id', $team->id)
            ->orWhere('team2_id', $team->id)
            ->finished()
            ->orderBy('starting_at', 'desc')
            ->limit(10)
            ->get();

        return view('teams.show', compact('team', 'last10_matches'));
    }
}
