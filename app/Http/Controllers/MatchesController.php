<?php

namespace App\Http\Controllers;

use App\LeagueModel;
use App\MatchModel;
use Illuminate\Http\Request;

class MatchesController extends Controller
{
    public function index(Request $request)
    {
        $leagues_collection = LeagueModel::all();
        $years = array_unique($leagues_collection->pluck('year')->toArray());
        $leagues = array_unique($leagues_collection->pluck('name', 'code')->toArray());

        $matches_q = MatchModel::with('times', 'team1', 'team2', 'league')
            ->orderBy('starting_at', 'desc');
        if ($request->get('starting_at')) {
            $matches_q->whereRaw('date(starting_at)=?', $request->get('starting_at'));
        }
        if ($request->get('scope') == 'upcoming') {
            $title = 'Upcoming Matches';
            $matches_q->upcoming();
        } else {
            $year = $request->get('year', config('bundes.default_year'));
            $league = $request->get('code', config('bundes.default_league'));
            $league = LeagueModel::where('year', $year)->where('code', $league)->first();
            $matches_q->where('league_id', $league->id);

            $title = $league->name;
        }

        $matches = $matches_q->paginate(20);

        return view('matches.index', compact('matches', 'title', 'years', 'leagues'));
    }
}
