<?php

namespace App\Http\Controllers;

use App\GoalgetterModel;
use App\LeagueModel;
use Illuminate\Http\Request;

class GoalgettersController extends Controller
{
    public function index(Request $request)
    {
        $leagues_collection = LeagueModel::all();
        $years = array_unique($leagues_collection->pluck('year')->toArray());
        $leagues = array_unique($leagues_collection->pluck('name', 'code')->toArray());

        $goalgetters_q = GoalgetterModel::orderBy('goals', 'desc');
        if ($request->get('name')) {
            $goalgetters_q->where('name', 'like', '%' . $request->get('name') . '%');
        }

        $year = $request->get('year', config('bundes.default_year'));
        $league_code = $request->get('code', config('bundes.default_league'));
        $league = LeagueModel::where('year', $year)->where('code', $league_code)->first();
        $goalgetters_q->where('league_id', $league->id);

        $goalgetters = $goalgetters_q->paginate(20);

        return view('goalgetters.index', compact('goalgetters', 'years', 'leagues'));
    }
}
