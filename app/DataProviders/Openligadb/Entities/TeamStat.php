<?php

namespace App\DataProviders\Openligadb\Entities;

class TeamStat extends Entity
{
    public $won;
    public $lost;
    public $draws;
    public $goals;
    public $matches;
    public $opponent_goals;
    public $points;
    public $ref_id;

    /**
     * Accepts the array data from the api
     *
     * @param array $team_data
     */
    public function __construct($team_data)
    {
        $this->fill($team_data);
    }

    protected function fill($team_data)
    {
        $this->ref_id = $team_data['TeamInfoId'];
        $this->won = $team_data['Won'];
        $this->lname = $team_data['TeamName'];
        $this->name = $team_data['ShortName'];
        $this->lost = $team_data['Lost'];
        $this->draws = $team_data['Draw'];
        $this->goals = $team_data['Goals'];
        $this->matches = $team_data['Matches'];
        $this->opponent_goals = $team_data['OpponentGoals'];
        $this->points = $team_data['Points'];
    }
}
