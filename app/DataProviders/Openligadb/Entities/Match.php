<?php

namespace App\DataProviders\Openligadb\Entities;

use App\DataProviders\Openligadb\Exceptions\InvalidMatchException;
use Carbon\Carbon;

class Match extends Entity
{
    public $team1_id;
    public $team2_id;
    public $city;
    public $stadium;
    public $ref_id;
    public $starting_at;
    public $is_finished;
    public $times = [];

    /**
     * Accepts the array dat from the api
     *
     * @param array $league_data
     */
    public function __construct($match_data)
    {
        $this->fill($match_data);
    }

    protected function fill($match_data)
    {
        $this->league_name = $match_data['LeagueName'];
        $this->team1_id = $match_data['Team1']['TeamId'];
        $this->team2_id = $match_data['Team2']['TeamId'];
        $this->is_finished = $match_data['MatchIsFinished'];
        $this->city = $match_data['Location']['LocationCity'];
        $this->stadium = $match_data['Location']['LocationStadium'];
        $this->ref_id = $match_data['MatchID'];
        $this->starting_at = Carbon::createFromFormat('Y-m-d H:i:s', str_replace('T', ' ', $match_data['MatchDateTime']));

        if ($this->is_finished) {
            //The match is finished so we need to have atleast 2 time periods, if we dont - throw exception
            if (empty($match_data['MatchResults'])) {
                throw new InvalidMatchException('FInished match doesnt have periods', 1);
            } else {
                foreach ($match_data['MatchResults'] as $key => $match_time) {
                    $this->times[] = [
                        'team1_score' => $match_time['PointsTeam1'],
                        'team2_score' => $match_time['PointsTeam2'],
                        'type' => $match_time['ResultTypeID']
                    ];
                }
            }
        }
    }
}
