<?php

namespace App\DataProviders\Openligadb\Entities;

class Entity
{
    /**
     * Fills the entity
     *
     * @return void
     */
    protected function fill($data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }
}
