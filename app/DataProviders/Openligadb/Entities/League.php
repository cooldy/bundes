<?php

namespace App\DataProviders\Openligadb\Entities;

class League extends Entity
{
    public $code;
    public $name;
    public $year;

    /**
     * Accepts the array dat from the api
     *
     * @param array $league_data
     */
    public function __construct($league_data)
    {
        $this->fill($league_data);
    }
}
