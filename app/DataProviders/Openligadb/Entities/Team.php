<?php

namespace App\DataProviders\Openligadb\Entities;

class Team extends Entity
{
    public $long_name;
    public $short_name;
    public $team_icon_url;
    public $ref_id;

    /**
     * Accepts the array data from the api
     *
     * @param array $team_data
     */
    public function __construct($team_data)
    {
        $this->fill($team_data);
    }

    protected function fill($team_data)
    {
        $this->long_name = $team_data['TeamName'];
        $this->short_name = $team_data['ShortName'];
        $this->team_icon_url = $team_data['TeamIconUrl'];
        $this->ref_id = $team_data['TeamId'];
    }
}
