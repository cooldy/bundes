<?php

namespace App\DataProviders\Openligadb\Entities;

class GoalGetter extends Entity
{
    public $name;
    public $goals;
    public $ref_id;

    /**
     * Accepts the array data from the api
     *
     * @param array $team_data
     */
    public function __construct($team_data)
    {
        $this->fill($team_data);
    }

    protected function fill($team_data)
    {
        $this->ref_id = $team_data['GoalGetterId'];
        $this->name = $team_data['GoalGetterName'];
        $this->goals = $team_data['GoalCount'];
    }
}
