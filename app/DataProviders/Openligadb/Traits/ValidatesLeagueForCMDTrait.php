<?php

namespace App\DataProviders\Openligadb\Traits;

/**
 * Used for validation on commands for the Openligadb CLient
 */
trait ValidatesLeagueForCMDTrait
{
    protected function validateParameters($leagues_collection)
    {
        $valid_years = $leagues_collection->pluck('year')->toArray();
        $valid_leagues = $leagues_collection->pluck('code')->toArray();
        $valid_leagues[] = 'all';

        $valid_years[] = 'all';

        if (!in_array($this->argument('year'), $valid_years)) {
            throw new \Exception('Invalid year', 1);
        }
        if (!in_array($this->argument('league'), $valid_leagues)) {
            throw new \Exception('Invalid league', 1);
        }
    }
}
