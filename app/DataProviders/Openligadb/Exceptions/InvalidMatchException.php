<?php

namespace App\DataProviders\Openligadb\Exceptions;

use Exception;

class InvalidMatchException extends Exception
{
}
