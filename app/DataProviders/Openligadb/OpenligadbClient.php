<?php

namespace App\Bundesliga;

use App\DataProviders\Openligadb\Entities\GoalGetter;
use App\DataProviders\Openligadb\Entities\League;
use App\DataProviders\Openligadb\Entities\Match;
use App\DataProviders\Openligadb\Entities\Team;
use App\DataProviders\Openligadb\Entities\TeamStat;
use App\DataProviders\Openligadb\Exceptions\InvalidMatchException;
use GuzzleHttp\Client as GuzzleClient;

class OpenligadbClient
{
    const BASE_URL = 'https://www.openligadb.de/api/';
    const AVAILABLE_LEAGUES = [
        'bl1',
        'bl2',
        'bl3'
    ] ;
    private $client;

    public function __construct()
    {
        $this->client = new GuzzleClient([
            'base_uri' => self::BASE_URL,
            'defaults' => [
                 'headers' => [
                    'content-type' => 'application/json',
                    'Accept' => 'application/json'
                ],
            ],
        ]);
    }

    /**
     * Gets teams from the api for a league and season
     *
     * @param string $league_code
     * @param int $year
     * @return array
s     */
    public function getTeams($league_code, $year)
    {
        $result = $this->client->request('GET', 'getavailableteams/' . $league_code . '/' . $year);
        $teams_arr_raw = json_decode($result->getBody(), 1);
        $teams = [];
        foreach ($teams_arr_raw as  $team) {
            $teams[] = new Team($team);
        }
        return $teams;
    }

    /**
     * Gets matches from the api by league code and season year
     *
     * @param string $league_code
     * @param int $year
     * @return array
     * @throws InvalidMatchException
     */
    public function getMatches($league_code, $year)
    {
        $result = $this->client->request('GET', 'getmatchdata/' . $league_code . '/' . $year);
        $matches_arr_raw = json_decode($result->getBody(), 1);
        $matches = [];
        foreach ($matches_arr_raw as $key => $match) {
            $matches[] = new Match($match);
        }
        return $matches;
    }

    /**
     * Gets the league info from the matches rq
     *
     * @param string $league_code
     * @param int $year
     * @return App\DataProviders\Openligadb\Entities\League
     */
    public function getLeagueInfo($league_code, $year)
    {
        $matches = $this->getMatches($league_code, $year);
        if (!empty($matches)) {
            return new League([
                'name' => $matches[0]->league_name,
                'code' => $league_code,
                'year' => $year
            ]);
        }
        return $matches;
    }

    /**
     * Gets team stats based on league/year info
     *
     * @param string $league_code
     * @param int $year
     * @return array
     */
    public function getTeamStats($league_code, $year)
    {
        $result = $this->client->request('GET', 'getbltable/' . $league_code . '/' . $year);
        $stats = json_decode($result->getBody(), 1);
        $stats_entities = [];
        foreach ($stats as  $stat) {
            $stats_entities[] = new TeamStat($stat);
        }
        return $stats_entities;
    }

    /**
     * Gets goalgetters info based on league/year info
     *
     * @param string $league_code
     * @param int $year
     * @return array
     */
    public function getGoalGetters($league_code, $year)
    {
        $result = $this->client->request('GET', 'getgoalgetters/' . $league_code . '/' . $year);
        $goal_getters = json_decode($result->getBody(), 1);
        $goal_getters_entities = [];
        foreach ($goal_getters as  $goal_getter) {
            $goal_getters_entities[] = new GoalGetter($goal_getter);
        }
        return $goal_getters_entities;
    }
}
