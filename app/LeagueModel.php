<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeagueModel extends Model
{
    protected $table = 'leagues';
}
