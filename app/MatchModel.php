<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MatchModel extends Model
{
    protected $table = 'matches';
    protected $guarded = []; //yolo

    public function scopeUpcoming($q)
    {
        return $q->where('is_finished', 0);
    }

    public function scopeFinished($q)
    {
        return $q->where('is_finished', 1);
    }

    public function team1()
    {
        return $this->hasOne(TeamModel::class, 'id', 'team1_id');
    }

    public function team2()
    {
        return $this->hasOne(TeamModel::class, 'id', 'team2_id');
    }

    public function league()
    {
        return $this->hasOne(LeagueModel::class, 'id', 'league_id');
    }

    public function times()
    {
        return $this->hasMany(MatchTimeModel::class, 'match_id', 'id');
    }
}
