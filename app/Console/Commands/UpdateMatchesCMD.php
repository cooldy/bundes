<?php

namespace App\Console\Commands;

use App\Bundesliga\OpenligadbClient;
use App\DataProviders\Openligadb\Traits\ValidatesLeagueForCMDTrait;
use App\LeagueModel;
use App\MatchModel;
use App\TeamModel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateMatchesCMD extends Command
{
    use ValidatesLeagueForCMDTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bundes:update_matches {league} {year}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates the matches for given  league/year';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(OpenligadbClient $client)
    {
        parent::__construct();
        $this->client = $client;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //get the teams so we have the database at hand to reduce sql requests
        $this->teams = TeamModel::all();
        $leagues_collection = LeagueModel::select('code', 'year')->get();
        try {
            $this->validateParameters($leagues_collection);
        } catch (\Throwable $th) {
            $this->error($th->getMessage());
            die;
        }

        if ($this->argument('league') == 'all' || $this->argument('year') == 'all') {
            $league_codes = $this->argument('league') == 'all' ? $leagues_collection->pluck('code')->toArray() : [$this->argument('league')];
            $seasons = $this->argument('year') == 'all' ? $leagues_collection->pluck('year')->toArray() : [$this->argument('year')];

            foreach ($league_codes as $league_code) {
                foreach ($seasons as $season) {
                    $this->updateMatches($league_code, $season);
                    sleep(0.1);
                }
            }
        } else {
            $this->updateMatches($this->argument('league'), $this->argument('year'));
        }
        $this->info('finished');
    }

    private function updateMatches($league_code, $season)
    {
        $matches = $this->client->getMatches($league_code, $season);
        $league = LeagueModel::where('code', $league_code)
            ->where('year', $season)->first();
        foreach ($matches as $match) {
            $match_model = MatchModel::firstOrNew([
                'ref_id' => $match->ref_id,
            ]);
            if ($match_model->exists && $match_model->is_finished) {
                //skip finished matches
                continue;
            }
            $team1 = $this->teams->where('ref_id', $match->team1_id)->first();
            $team2 = $this->teams->where('ref_id', $match->team2_id)->first();
            if (!$team1 || !$team2) {
                Log::warning('Team with ref_id  not found, skipping match ref_id ' . $match->ref_id);
                continue;
            }
            $match_model->league_id = $league->id;
            $match_model->team1_id = $team1->id;
            $match_model->team2_id = $team2->id;
            $match_model->city = $match->city;
            $match_model->stadium = $match->stadium;
            $match_model->ref_id = $match->ref_id;
            $match_model->starting_at = $match->starting_at;
            $match_model->is_finished = $match->is_finished;
            $match_model->save();

            //clear the old time info
            DB::table('matches_times')->where('match_id', $match_model->id)->delete();
            foreach ($match->times as $time) {
                DB::table('matches_times')->insert([
                    'team1_score' => $time['team1_score'],
                    'team2_score' => $time['team2_score'],
                    'type' => $time['type'],
                    'match_id' => $match_model->id,
                ]);
            }
        }
    }
}
