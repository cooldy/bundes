<?php

namespace App\Console\Commands;

use App\Bundesliga\OpenligadbClient;
use App\LeagueModel;
use Illuminate\Console\Command;

class GetLeaguesBruteforceCMD extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bundes:get_leagues';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gets all the leagues (needed for names and validation) from the matches requests by league and league year (parameters as array)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(OpenligadbClient $client)
    {
        $seasons = [2016, 2017, 2018, 2019];
        $league_codes = ['bl1', 'bl2', 'bl3'];
        foreach ($league_codes as $league_code) {
            foreach ($seasons as $season) {
                $league = $client->getLeagueInfo($league_code, $season);

                $league_model = new LeagueModel();
                $league_model->code = $league->code;
                $league_model->name = $league->name;
                $league_model->year = $league->year;
                $league_model->save();
                sleep(0.1);
            }
        }
    }
}
