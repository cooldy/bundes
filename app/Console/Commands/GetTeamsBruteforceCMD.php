<?php

namespace App\Console\Commands;

use App\Bundesliga\OpenligadbClient;
use App\TeamModel;
use Illuminate\Console\Command;

class GetTeamsBruteforceCMD extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bundes:get_teams';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gets all the teams by the given league and league year (parameters as array)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(OpenligadbClient $client)
    {
        $seasons = [2016, 2017, 2018, 2019];
        $league_codes = ['bl1', 'bl2', 'bl3'];
        foreach ($league_codes as $league_code) {
            foreach ($seasons as $season) {
                $teams = $client->getTeams($league_code, $season);
                foreach ($teams as $team) {
                    $team_model = TeamModel::where('ref_id', $team->ref_id)->first();

                    //only insert new teams, skip the existing
                    if ($team_model) {
                        continue;
                    }
                    $team_model = new TeamModel();
                    $team_model->long_name = $team->long_name;
                    $team_model->short_name = $team->short_name;
                    $team_model->team_icon_url = $team->team_icon_url;
                    $team_model->ref_id = $team->ref_id;
                    $team_model->save();
                }
                sleep(0.1);
            }
        }
    }
}
