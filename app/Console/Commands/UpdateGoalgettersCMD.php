<?php

namespace App\Console\Commands;

use App\Bundesliga\OpenligadbClient;
use App\DataProviders\Openligadb\Traits\ValidatesLeagueForCMDTrait;
use App\GoalgetterModel;
use App\LeagueModel;
use Illuminate\Console\Command;

class UpdateGoalgettersCMD extends Command
{
    use ValidatesLeagueForCMDTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bundes:update_goalgetters {league} {year}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates the goalgetters for given  league/year';

    /**

     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(OpenligadbClient $client)
    {
        parent::__construct();
        $this->client = $client;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $leagues_collection = LeagueModel::select('code', 'year')->get();
        try {
            $this->validateParameters($leagues_collection);
        } catch (\Throwable $th) {
            $this->error($th->getMessage());
            die;
        }

        if ($this->argument('league') == 'all' || $this->argument('year') == 'all') {
            $league_codes = $this->argument('league') == 'all' ? $leagues_collection->pluck('code')->toArray() : [$this->argument('league')];
            $seasons = $this->argument('year') == 'all' ? $leagues_collection->pluck('year')->toArray() : [$this->argument('year')];

            foreach ($league_codes as $league_code) {
                foreach ($seasons as $season) {
                    $this->updateGoalgetters($league_code, $season);
                    sleep(0.1);
                }
            }
        } else {
            $this->updateGoalgetters($this->argument('league'), $this->argument('year'));
        }
        $this->info('finished');
    }

    private function updateGoalgetters($league_code, $season)
    {
        $goalgetters = $this->client->getGoalGetters($league_code, $season);
        $league = LeagueModel::where('code', $league_code)
            ->where('year', $season)->first();
        foreach ($goalgetters as $goalgetter) {
            $goalgetter_model = GoalgetterModel::firstOrNew([
                'ref_id' => $goalgetter->ref_id,
            ]);
            $goalgetter_model->name = $goalgetter->name;
            $goalgetter_model->goals = $goalgetter->goals;
            $goalgetter_model->league_id = $league->id;
            $goalgetter_model->save();
        }
    }
}
