<?php

namespace App\Console\Commands;

use App\Bundesliga\OpenligadbClient;
use App\DataProviders\Openligadb\Traits\ValidatesLeagueForCMDTrait;
use App\LeagueModel;
use App\TeamLeagueStatModel;
use App\TeamModel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class UpdateTeamStatsCMD extends Command
{
    use ValidatesLeagueForCMDTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bundes:update_teams_stats {league} {year}';

    /**
     * The console command description.
     *
     * @var string1
     */
    protected $description = 'Updates teams statistics based on league/year';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(OpenligadbClient $client)
    {
        parent::__construct();
        $this->client = $client;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $leagues_collection = LeagueModel::select('code', 'year')->get();

        try {
            $this->validateParameters($leagues_collection);
        } catch (\Throwable $th) {
            $this->error($th->getMessage());
            die;
        }

        if ($this->argument('league') == 'all' || $this->argument('year') == 'all') {
            $league_codes = $this->argument('league') == 'all' ? $leagues_collection->pluck('code')->toArray() : [$this->argument('league')];
            $seasons = $this->argument('year') == 'all' ? $leagues_collection->pluck('year')->toArray() : [$this->argument('year')];
            foreach ($league_codes as $league_code) {
                foreach ($seasons as $season) {
                    $this->updateTeamStats($league_code, $season);
                    sleep(0.1);
                }
            }
        } else {
            $this->updateTeamStats($this->argument('league'), $this->argument('year'));
        }
        $this->info('finished');
    }

    private function updateTeamStats($league_code, $season)
    {
        $team_stats = $this->client->getTeamStats($league_code, $season);
        $league = LeagueModel::where('code', $league_code)
            ->where('year', $season)->first();
        foreach ($team_stats as $team_stat) {
            $team = TeamModel::where('ref_id', $team_stat->ref_id)->first();
            if (!$team) {
                Log::warning('Team with ref_id ' . $team_stat->ref_id . 'not found');
                continue;
            }
            $team_stats_model = TeamLeagueStatModel::firstOrNew([
                'league_id' => $league->id,
                'team_id' => $team->id
            ]);
            $team_stats_model->won = $team_stat->won;
            $team_stats_model->lost = $team_stat->lost;
            $team_stats_model->draws = $team_stat->draws;
            $team_stats_model->goals = $team_stat->goals;
            $team_stats_model->matches = $team_stat->matches;
            $team_stats_model->opponent_goals = $team_stat->opponent_goals;
            $team_stats_model->points = $team_stat->points;
            $team_stats_model->save();
        }
    }
}
