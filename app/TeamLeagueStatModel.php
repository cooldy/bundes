<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamLeagueStatModel extends Model
{
    protected $guarded = [];//yolo
    protected $table = 'teams_leagues_stats';

    public function team()
    {
        return $this->belongsTo(TeamModel::class);
    }
}
