<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MatchTimeModel extends Model
{
    protected $table = 'matches_times';

    public function getTypeLabel()
    {
        return $this->type == 1 ? 'half time' : 'final time';
    }
}
