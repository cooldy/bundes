@extends('layout.app')
@section('content')
<div class="container">

    <h2>{{$team->long_name}}</h2>

    <div class="container">
        <h4>Team Info</h4>
        <img width="100 "src="{{$team->team_icon_url}}" alt=""> {{$team->long_name}} {{$team->short_name ? "(".$team->short_name.")" : ""}}


        <h4>Last 10 matches</h4>
        @include('matches.table',['matches'=>$last10_matches])
    </div>
</div>

@endsection
    
@section('scripts')
<script>
    $(document).ready(function(){
        $('#starting_at').datepicker({
            autoclose:true,
            format:'yyyy-mm-dd'
        });
    })
</script>
@endsection