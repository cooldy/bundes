@extends('layout.app')
@section('content')
<div class="container">

    <h2>{{$title}}</h2>

    <div>
        <form action="" class="row">
            @include('common.league_filters')
            <div class="col-sm-1">
                <label for="">&nbsp;</label>
                <button class="btn btn-primary">Search</button>
            </div>
        </form>
    </div>
    <table class="table table-responsive">
        <thead>
            <tr>
                <td>Team</td>
                <td>Matches</td>
                <td>Wins</td>
                <td >Losses</td>
                <td>Draws</td>
                <td >Win ratio</td>
                <td>Points</td>
                <td>Goals striked</td>
                <td>Goals received</td>
            </tr>
        </thead>
        <tbody>
            @forelse ($teams_stats as $team_stat)
            <tr>
                <td>
                    <a href="{{route('teams.show',$team_stat->team_id)}}">
                        <img src="{{$team_stat->team->team_icon_url}}" width="20" alt=""> 
                        {{$team_stat->team->long_name}}
                    </a>
                </td>
                <td>{{$team_stat->matches}}</td>
                <td>{{$team_stat->won}}</td>
                <td >{{$team_stat->lost}}</td>
                <td>{{$team_stat->draws}}</td>
                @if(!$team_stat->matches)
                <td >-</td>
                @else
                <td >{{number_format(($team_stat->won/$team_stat->matches*100),2)}}%</td>
                @endif
                <td>{{$team_stat->points}}</td>
                <td>{{$team_stat->goals}}</td>
                <td>{{$team_stat->opponent_goals}}</td>
            </tr>
            @empty
            <tr>
                <td colspan="3" class="text-center">No teams found for the current criteria </td>
            </tr>        
            @endforelse
        </tbody>
    </table>
    {{$teams_stats->links()}}
</div>

@endsection
    
@section('scripts')
<script>
    $(document).ready(function(){
        $('#starting_at').datepicker({
            autoclose:true,
            format:'yyyy-mm-dd'
        });
    })
</script>
@endsection