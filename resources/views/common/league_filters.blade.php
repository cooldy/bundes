<div class="col-sm-3">
    <label for="starting_at">Year</label>
    <select name="year" id="" class="form-control">
        @foreach ($years as $year)
            <option {{Request::get('year',config('bundes.default_year'))==$year ? 'selected' :""}} value="{{$year}}">{{$year}}</option>
        @endforeach 
    </select>
</div>
<div class="col-sm-3">
    <label for="starting_at">League</label>
    <select name="code" id="" class="form-control">
        @foreach ($leagues as $code=>$name)
            <option {{Request::get('code',config('bundes.default_league'))==$code ? 'selected' :""}} value="{{$code}}">{{$name}}</option>
        @endforeach 
    </select>
</div>
