<table class="table table-responsive">
    <thead>
        <tr>
            <td>Team 1</td>
            <td></td>
            <td >Team 2</td>
            <td >League</td>
            <td>Starting Time</td>
        </tr>
    </thead>
    <tbody>
        @forelse ($matches as $match)
        <tr>
            <td>
                <a href="{{route('teams.show',$match->team1_id)}}">
                    <img src="{{$match->team1->team_icon_url}}" width="20" alt=""> 
                    {{$match->team1->long_name}}
                </a>
            </td>
            <td>VS</td>
            <td>
                <a href="{{route('teams.show',$match->team2_id)}}">
                    <img src="{{$match->team2->team_icon_url}}" width="20" alt="">  {{$match->team2->long_name}}
                </a>
            </td>

            <td>{{$match->league->name}}</td>
            <td>{{$match->starting_at}}</td>
        </tr>
        @if($match->is_finished)
        <tr>
            <td colspan="52" >
                <table class="col-sm-6">
                    @foreach ($match->times as $time)   
                    <tr>
                        <td>{{$time->team1_score}}</td>
                        <td>{{$time->team2_score}}</td>
                        <td >{{$time->getTypeLabel()}}</td>
                    </tr>
                    @endforeach
                </table>
            </td>
        </tr>
        @endif
        @empty
        <tr>
            <td colspan="3" class="text-center">No matches for the current criteria </td>
        </tr>        
        @endforelse
    </tbody>
</table>