@extends('layout.app')
@section('content')
<div class="container">

    <h2>{{$title}}</h2>

    <div>
        <form action="" class="row">
            <div class="col-sm-3">
                <label for="starting_at">Starting at</label>
                <input value="{{Request::get('starting_at')}}" id="starting_at" name="starting_at" type="text" class="form-control">
            </div>
            @if(!Request::get('scope')=='upcoming')
            @include('common.league_filters')
            @endif


            <div class="col-sm-1">
                <label for="">&nbsp;</label>
                <button class="btn btn-primary">Search</button>
            </div>
        </form>
    </div>
    @include('matches.table')
    {{$matches->links()}}
</div>

@endsection
    
@section('scripts')
<script>
    $(document).ready(function(){
        $('#starting_at').datepicker({
            autoclose:true,
            format:'yyyy-mm-dd'
        });
    })
</script>
@endsection