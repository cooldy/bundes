@extends('layout.app')
@section('content')
<div class="container">

    <h2>Goalgetters 
        @if(Request::get('name'))
        (searching for {{Request::get('name')}})
        @endif
    </h2>

    <div>
        <form action="" class="row">
            <div class="col-sm-3">
                <label for="starting_at">Goalgetter name</label>
                <input value="{{Request::get('name')}}" id="name" name="name" type="text" class="form-control">
            </div>
            @include('common.league_filters')
            <div class="col-sm-1">
                <label for="">&nbsp;</label>
                <button class="btn btn-primary">Search</button>
            </div>
        </form>
    </div>
    <table class="table table-responsive">
        <thead>
            <tr>
                <td>Goalgetter</td>
                <td>Goals</td>
            </tr>
        </thead>
        <tbody>
            @forelse ($goalgetters as $goalgetter)
            <tr>
                <td>{{$goalgetter->name}}</td>
                <td>{{$goalgetter->goals}}</td>
            </tr>
            @empty
            <tr>
                <td colspan="3" class="text-center">No goalgetters found for the current criteria </td>
            </tr>        
            @endforelse
        </tbody>
    </table>
    {{$goalgetters->links()}}
</div>

@endsection