<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MatchesController@index')->name('home');
Route::get('/matches', 'MatchesController@index')->name('matches.index');
Route::get('/teams', 'TeamsController@index')->name('teams.index');
Route::get('/teams/{id}', 'TeamsController@show')->name('teams.show');
Route::get('/goalgetters', 'GoalgettersController@index')->name('goalgetters.index');
